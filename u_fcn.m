function [ u ] = u_fcn( t, fcn_num )
%U_FCN Control function for the burgers equation
%   Implements a control function to be used in the burgers equation
%
%   Inputs
%     t       - values where to evaluate the function
%     fcn_num - number of the function being selected
%       Currently defined functions:
%          1 - cos(\pi*t)
%          2 - 2sin(\pi*t)
%          3 - step function
%          4 - narrow sawtooth
%          5 - wide sawtooth
%          6 - hat function
%          7 - exponential decay
%          8 - trig + expontial
%          9 - multiple trig functions
%         10 - polynomial (degree 7)
%         11 - polynomial (degree 8)
%   Output
%     u       - value of u(t) for given t
%
% Alan Lattimer, Virginia Tech 2015
%--------------------------------------------------------------------------

switch fcn_num
  case 1
    u = cos(pi.*t);
  case 2
    u = 2*sin(pi.*t);
  case 3
    u = zeros(size(t));
    for k = 1:length(t)
      if (t(k) < 2) || (t(k) > 8)
        u(k) = 1;
      elseif (t(k) > 3) && (t(k) < 5)
        u(k) = -0.5;
      elseif (t(k) >=5) && (t(k) < 6)
        u(k) = 0.6;
      elseif (t(k) >=6) && (t(k) < 7)
        u(k) = 1.8;
      end
    end
  case 4
    u = mod(t,1);
  case 5
    u = 0.5*mod(t,2);
  case 6
    u = zeros(size(t));
    for k = 1:length(t)
      if mod(t(k),4) < 2
        u(k) = 1 - 0.5*mod(t(k),2);
      else
        u(k) = 0.5*mod(t(k),2);
      end
    end
  case 7
    u = exp(-t./pi);
  case 8
    u = 4.*exp(-t./pi).*cos(pi.*t);
  case 9
    u = cos(pi*t) + sin(3*pi*t) + 0.5*sin((pi/3)*t);
  case 10
%     P = [ -0.000013168961570,  0.000658448078524,  -0.014068840611127, ...
%            0.167684777330743, -1.220772980108906,   5.588336635509152, ...
%          -15.955745514965995, 27.151716192078219, -24.669554878260243, ...
%            8.983598952552709,  0.062500000000000];
    P = [ -0.001519632916667,   0.052942050000000,  -0.729703916666667, ...
           5.042100000000000, -18.146129166666668,  31.384500000000003, ...
         -18.765000000000004,  -1.000000000000000];
    u = polyval(P,t);
  case 11
    P = -poly([0.5,2,4,8,10])./100;
    u = polyval(P,t);
  otherwise
    u = ones(size(t));
end


end

