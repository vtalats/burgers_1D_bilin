# Create Bilinear system for Burgers 

## `u_fcn.m`

This provides for various input functions to the burgers equation.

## `burgers_1D_bd.m`

Solves the Burgers' equation using a backward difference scheme.

## `build_burgers_mat.m`

This builds and runs the model for Burger's equation.  The output 
generated is stored in the file `burgers_result_<n>.mat`. The system
matrices for a quadratic discretization are output to a file 
`quad_matrices_burgers_<n>.mat` where n is the discretization size.
The quadratic discretized form is given by

```
    w_t = Aw + H(w o w) + Nwu(t) + bu(t)
      y = Cw
```

Further, if n is small (<300), a Carleman bilinearization is computed 
to convert the quadratic system to a bilinear system.  These matrices are
stored in the file `bilin_matrices_burgers_<n>.mat`. The bilinear
discretized form is given by

```
    wb_t = Ab*wb + Nb*wb*u(t) + bb*u(t)
      yb = cb*wb
```

where

```
Ab = [A     H  ], Nb = [   N      0  ], bb = [b], cb = [c 0], wb = [  w  ]
     [0  L(A,I)]       [L(b,I) L(N,I)]       [0]                   [w o w]
```

and

```
L(A,B) = (A o B) + (B o A)
```
