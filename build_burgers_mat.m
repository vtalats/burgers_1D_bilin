%=========================================================================================
% build_burgers_mat.m

% This builds and runs the model for Burger's equation.  The output
% generated is stored in the file 'burgers_result_<n>.mat'. The system
% matrices for a quadratic discretization are output to a file
% 'quad_matrices_burgers_<n>.mat' where n is the discretization size.
% The quadratic discretized form is given by
%     w_t = Aw + H(w o w) + Nwu(t) + bu(t)
%       y = Cw

% Further, if n is small (<300), a Carleman bilinearization is computed
% to convert the quadratic system to a bilinear system.  These matrices are
% stored in the file 'bilin_matrices_burgers_<n>.mat'. The bilinear
% discretized form is given by
%     wb_t = Ab*wb + Nb*wb*u(t) + bb*u(t)
%       yb = cb*wb
% where
% Ab = [A     H  ], Nb = [   N      0  ], bb = [b], cb = [c 0], wb = [  w  ]
%      [0  L(A,I)]       [L(b,I) L(N,I)]       [0]                   [w o w]
% and
% L(A,B) = (A o B) + (B o A)
%
%-----------------------------------------------------------------------------------------
%
% Alan Lattimer, 2015, Virginia Tech
%   Sep 2016 - Added Carleman bilinearization
%
%=========================================================================================


close all
clear


fprintf('Running the model to build full matrices for test.\n');
t_span     = linspace(0,10,500);
p          = @(x) zeros(size(x));
n          = 100;
nu         = 0.01;
ctrl_type  = 1;

bounds     = [0 1];
quad_file  = sprintf('quad_matrices_burgers_%d.mat',n);
out_file   = sprintf('burgers_result_%d.mat',n);
bilin_file = sprintf('bilin_matrices_burgers_%d.mat',n);

[ A, H, Q, N, b, c, E, Y, T, x, W ] = burgers_1D_bd( bounds, n, nu, p, ctrl_type, t_span );


%% Compute alternate quadratic matrix
% H(abs(H)<=eps)=0;
% [p,m] = size(H);
% fprintf('Build Hf:\n')
% tic
% II = 1:p;
% JJ = 1:m+1:m^2;
% c2f = sparse(II,JJ,1);
% Q = H*c2f;
% clear II JJ c2f
% [II,JJ,VV] = find(H);
% JJ = p*(II-1) + JJ;
% Q2 = sparse(II,JJ,VV);
% toc

% dH = diag(diag(H));
% [p,m] = size(H);
% II = 1:p;
% JJ =  1:m+1:m^2;
% c2f = sparse(II,JJ,1);
% Hf = H*c2f;
% clear II JJ c2f
% [II,JJ,VV] = find(H);
% JJ = p*(II-1) + JJ;
% Q = sparse(II,JJ,VV);


fprintf('Saving full quadradic matrices to %s.\n\n', quad_file);
save(quad_file, 'A', 'Q', 'N', 'b', 'c', 'x', 'n');
save(out_file, 't_span','Y');

if (n < 300)
%   h = (bounds(2)-bounds(1))/n;
%   d = (1/(4*h)).*ones(n,1);
%   Qtmp = spdiags([d -d],[-1 1],n,n);
%   [II,JJ,VV] = find(Qtmp);
%   clear Qtmp;
%
%   JJ = n*(JJ-1)+JJ;
%
%   Q = sparse(II,JJ,VV,n,n^2);
%

  fprintf('Generating a Carleman bilinearization.\n')

  % Create zero portions of the matrices
  A0  = sparse(n^2, n);
  N10 = sparse(n, n^2);
  N20 = sparse(n^2,n^2);
  b0  = sparse(n^2, 1);
  c0  = sparse(1, n^2);

  I  = speye(n);
  II = 2 * kron(I,I);

  % Compute the Kronecker products
  L_A  = kron(A,I) + kron(I,A);
  L_b  = kron(b,I)  + kron(I,b);
  L_N  = kron(N,I)  + kron(I,N);

  x    = x';
  xx   = kron(x,x);

  % Build Bilinear matrices
  Ab = [A,   Q; ...
        A0, L_A];
  Nb = [ N,   N10; ...
        L_b,  L_N];
  bb = [b;b0];
  cb = [c,c0];
  xb = [x;xx];
else
  fprintf('Not creating a bilinearization due to size constraints.\n')
end

%% Solve the Bilinear system

ub = @(t) u_fcn(t,1);
w0 = zeros(size(xb));

% Define the ODE based on the discretization
bilin_ode =@(t,w)  Ab*w + (Nb*w + bb)*ub(t);

% Solve the ODE
fprintf('Solving the differential equation.\n');
tic;
[Tb,Wb] = ode15s(bilin_ode,t_span,w0);
tf = toc;
fprintf('Completed in %f seconds.\n',tf);

% Plot the full solution W(T,x)
fprintf('Creating plots\n')
colormap('jet');
if (length(Tb) > 500)
  outidx = ceil(linspace(1,length(Tb),500));
else
  outidx = 1:length(Tb);
end
figure(11);
surf(x,Tb(outidx),Wb(outidx,1:n),'EdgeColor', 'none');

% Generate the output Y from the solution W
Yb = cb*Wb';
maxYb = max(Yb);

% Plot the output
fprintf('The max output is %f.\n',maxYb)
figure(12);
plot(Tb(outidx),Yb(outidx))
xlabel('Time (t)');
ylabel('y(t)');
